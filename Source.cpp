#include <iostream>
#include <vector>
using namespace std;


const int sizeBoard = 8;
int chessBoard[sizeBoard][sizeBoard];


struct {
	int x;
	int y;
} pos;


void Initialization();
void SetPoint();
void PrintResult();
int Solution(int x, int y, int xMove[sizeBoard], int yMove[sizeBoard], int move = 1);
bool isSafe(int x, int y);



int main() 
{
	int xMove[8] = { 2, 1, -1, -2, -2, -1, 1, 2 };
	int yMove[8] = { 1, 2, 2, 1, -1, -2, -2, -1 };

	Initialization();
	SetPoint();
	
	if (Solution(pos.x, pos.y, xMove, yMove) == 0) {
		cout << "Solution does not exist";
		return 0;
	}
	else
		PrintResult();

	return 0;
}



void Initialization()
{
	for (int i = 0; i < sizeBoard; i++)
		for (int j = 0; j < sizeBoard; j++)
			chessBoard[i][j] = -1;
}

void SetPoint()
{
	int x, y;
	cout << "Set position(x, y): ";
	cin >> x >> y;
	pos.x = x; pos.y = y;

	chessBoard[x][y] = 0;
}

int Solution(int x, int y, int xMove[sizeBoard], int yMove[sizeBoard], int move)
{
	int next_x, next_y;
	if (move == sizeBoard * sizeBoard)
		return 1;

	for (int i = 0; i < 8; i++) {
		next_x = x + xMove[i];
		next_y = y + yMove[i];
		if (isSafe(next_x, next_y)) {
			chessBoard[next_x][next_y] = move;
			if (Solution(next_x, next_y, xMove, yMove, move + 1) == 1)
				return 1;
			else
				chessBoard[next_x][next_y] = -1;
		}
	}
	return 0;
}

bool isSafe(int x, int y)
{
	return (x >= 0 && x < sizeBoard && y >= 0 && y < sizeBoard && chessBoard[x][y] == -1);
}

void PrintResult() 
{
	for (int i = 0; i < sizeBoard; i++) {
		for (int j = 0; j < sizeBoard; j++) {
			cout << chessBoard[i][j] << ' ';
		}
		cout << endl;
	}
}


